# Watch Out Fedi - Masto Here To Get You

2023-12-19

Over the last few days, there has been an influx (again) of spamming bots coming from mastodon.social, mastodon.online and mastodon.world. Mostly consisting of inappropriate content links claiming to be for OnlyFans, they ping a large number of users and leaving it at that. I've had a total of 3-4 on my main account ([alpha.polymaths.social](https://alpha.polymaths.social/@orbitalmartian)), as well as ~2 on my Akkomma alt account and a few on my Vivaldi Social account.

This isn't the first time that it's happened, but it's the first wave that I have actually been affected by. It is becoming annoying at this point, yet still funny to see that this is happening with no action from the admins over at Mastodon social/online/world. I have seen numerous posts from the maintainer of Mastodon and admin of these instances, doing other things such as welcoming Threads to "Mastodon" (wrong in it's own right, it's the Fediverse), and talking about the rate of these instances users following each other.

I have made a few toots about the issue:

- [One, reminding people to not be stupid and don't click on links you aren't expecting or aren't sure what it's for.](https://alpha.polymaths.social/@orbitalmartian/statuses/01HHZEM31ZSXPP6C9VMM1MS6QT)
- [Two, asking if my instance can block them. FYI we can't because the feature hasn't been added yet.](https://alpha.polymaths.social/@orbitalmartian/statuses/01HHZ5Z2G5YMMQ286WBZWSZV83)
- [Three, asking whether there was a user level domain blocking on GoToSocial - and we don't have it but it is now a planned feature because the single user software is being used for a large instance, polymaths.social (AKA the instance I'm on)](https://alpha.polymaths.social/@orbitalmartian/statuses/01HHTAMZ3Z855DVVQNSXN4AQ0S)

Anyway, I have had enough of moaning about this now, so I hope you have a wonderful Christmas and a happy New Year!