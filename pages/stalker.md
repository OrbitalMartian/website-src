# Stalker
Here is a list of my social links for all my stalkers :)

|  |  |
| [GoToSocial](https://alpha.polymaths.social/@orbitalmartian) | [Mastodon](https://linuxrocks.online/@orbitalmartian) |
| [Peertube](https://peertube.linuxrocks.online/) | [YouTube](https://youtube.com/@orbitalmartian) |
| [Bookwyrm](https://bookwyrm.social/@orbitalmartian) |
