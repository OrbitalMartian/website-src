# 11 - NaNoWriMo - What Is It? Am I Doing It?

2023-10-28

# **Clickety Click**
November is NaNoWriMo and I'm here to answer a couple of questions. What is it? And am I doing it?

## What Is It?
<a href="https://nanowrimo.org/">NaNoWriMo</a> is a annual month long writing challenge to write a total of 50,000 words on a story (typically new, I believe) throughout the month.

## Am I Doing It?
In short, <a href="https://nanowrimo.org/participants/orbitalmartian">yes</a>. In long, I am tentatively joining the challenge in order to better myself as an author/writer.

I'm really running low on blog ideas, this one was inspired by a comment to me by <a href="https://alpha.polymaths.social/@amin">Benjamin Hollon</a>. If you have any suggestions, <a href="https://alpha.polymaths.social/@orbitalmartian">hit me up on the Fediverse</a>>.