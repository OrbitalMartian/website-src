# Blogroll
## Here is a list of all the blogs and sites that I frequently read.

* [David Revoy](https://www.davidrevoy.com/) / [Pepper & Carrot](https://www.peppercarrot.com/)
* [The Linux Cast](https://thelinuxcast.org)
* Amin's [TTY1](https://tty1.blog/) / [Musings](https://benjaminhollon.com/musings/)
* [RL Dane](https://rldane.space/)
* [Ivan](https://libreivan.com/)
* [Adamsdesk](https://www.adamsdesk.com/)
* JP's [ModdedBear](https://moddedbear.com/)
* [Clayton Errington](https://claytonerrington.com/)