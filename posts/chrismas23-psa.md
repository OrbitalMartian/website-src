# Christmas Break With No Posts

2023-12-17

Good evening folks, welcome to another blog post, this is a quick PSA (Public Service Announcement).

I will not be posting any more scheduled posts until the New Year.

So until then, have a Merry Christmas and happy New Year!
