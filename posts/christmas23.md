# Merry Christmas All

2023-12-24

Good morning (at 00:00 here in the UK). It's now Christmas Eve and to allow myself to stay offline tomorrow and spend time with my family, I'm doing my Christmas wishing now.

Merry Christmas and if I don't catch you all before Happy New Year! This year has been crazy, I've grown a lot, 100+ followers on my Mastodon account and 50+ followers on my GoToSocial main account. 42 subscribers on YouTube (at the time of writing this) and around 6 on my Peertube account. Now I've done the numbers, let's move on, I have met so many great people who I think of as either friends or FediFriends, whether that's from flight sim Discord communities or from the Fediverse. On the Fediverse, [Amin](https://alpha.polymaths.social/@amin) created what is now the largest GoToSocial instance out there and I am lucky enough to have been a member since the very beginning (this is where majority of my actual interacting and getting to know FediFriends happened). It is through Polymaths.social that GtS (an intended single user software) is planning user level moderation features.

Throughout the year I became a moderator for my favourite flight simulator creator ([DreamsOfWings](https://youtube.com/@Dreamsofwings))'s Discord community and became a moderator for [LinuxRocks Peertube](https://peertube.limuxrocks.online). Further to this I also had my first driving lesson and grew in my flight sim ability.

Now I'm really tired now so checkout DreamsOfWings YT channel (linked above) and...

## HERE'S TO A WONDERFUL 2023 AND TO AN EVEN BETTER 2024!