# Learning Rust - Episode 1

2022-12-08

## Chapter 1 - Installing and Hello World

### Introduction
I started learning Rust thanks to Francesco Ciull and his first livestream in the Rust From Zero series on his YouTube channel on Thursday 8th December 2022. Rust is a fairly new programming language, having been released to the public in 2010.

#### Installing Rust
To get started we have to do a little bit of installing. To install on Windows is a bit harder so for this course, I'll be using Linux, you can follow these steps on Windows using WSL or by following the Windows install instructions in the book used. 

To install Rust, we use a tool called \`rustup\`. To install \`rustup\`, 

```bash
$ curl --proto '=https' --tlsv1.3 https://sh.rustup.rs -sSf | sh
```

This command downloads a script and starts the installation of the \`rustup\` tool, which installs the latest stable version of Rust. Password may be needed. If it was successful, this will appear:
```bash
Rust is installed now. Great!
```
#### Updating and Uninstall
To update and uninstall rustup, use the following commands:
```bash
$ rustup update
```
```bash
 $ rustup self uninstall
```
 For local documentation run \`rustup doc\`.

### Hello World Program
To start with we are going to be programming a Hello World program.
To do this we are going to use the following code: 
```rust
fn main() {
  println!("Hello, world!");
 }
```
---
Let's go through the code now.
```rust
fn main() {

}
```

This is a function called main. All the code for the function go inside the squigly brackets.
```rust
println!("Hello, world!"); 
```
This is the print command, the `!` tells us that it is text. The `;` on the end acts like it does in JavaScript, basically saying that the command has finished, and to move on.

### Last Words
I am going to be doing more articles about the process and majority of these articles will also be posted as videos to my [YouTube channel](https://www.youtube.com/channel/UCbBtLFxKBpcwt85EoP0kXRg). After an hour learning this much, I am enjoying myself too much to stop, I'm now personally on Chapter 3, and will get Chapter 2 written up about sometime between now and next week.

**Links:**
* [Rust Lang Book](https://doc.rust-lang.org/book/)
* [My Rust Crash Course Git Repo](https://github.com/orbitalmartian8/rust-crash-course)
* [Francesco Ciull's Rust Crash Course Git Repo - from Rust From Zero live series](https://github.com/FrancescoXX/rust-crash-course)